resource "aws_cloudwatch_metric_alarm" "logs_in_anomaly_detection" {
  alarm_name                = "logevents_incoming"
  comparison_operator       = "GreaterThanUpperThreshold"
  evaluation_periods        = "1"
  threshold_metric_id       = "ad1"
  alarm_actions             = [aws_sns_topic.ile_topic.arn]
  alarm_description         = "this alarm monitors IncomingLogEvents for anomaly"
  insufficient_data_actions = []

  metric_query {
    id          = "ad1"
    expression  = "ANOMALY_DETECTION_BAND(m1, 2)"
    label       = "IncomingLogEvents"
    return_data = "true"
  }
  metric_query {
    id          = "m1"
    return_data = "true"
    metric {
      metric_name = "IncomingLogEvents"
      namespace   = "AWS/Logs"
      period      = "900"
      stat        = "Average"
    }
  }
}

resource "aws_sns_topic" "ile_topic" {
  name         = "IncomingLogEventsAlarm"
  display_name = "IncomingLogEventsAlarm topic"
}
